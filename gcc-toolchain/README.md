# Build Linux/x86-64 toolchain with GCC and binutils from source.

Require make, cmake and GCC to build this toolchain.

1. Create config.make:

- Define GCC-SOURCE-DIR which should point to the GCC source tree cloned from [GCC git repo](https://gcc.gnu.org/git/gitweb.cgi?p=gcc.git)

- Define BINUTILS-SOURCE-DIR which should point to the binutils source tree cloned from [binutils git repo](https://sourceware.org/git/?p=binutils-gdb.git)

- Add "CMAKE-FLAGS+=-DCMAKE_BUILD_TYPE=Debug" to enable debug build.

- Add "LANGS=default" to enable default languages.

- Add "CMAKE-FLAGS+=-DBINUTILS-ENABLE-TESTS=true" to run binutils testsuite.

- Add "CMAKE-FLAGS+=-DGCC-ENABLE-TESTS=true" to run GCC testsuite.

- Add "CMAKE-FLAGS+=-DTOOLCHAIN-INSTALL=install-strip" to strip debug info when installing toolchain.

- Add CC= and CXX= in config.make to use alternate compiler to build toolchain if needed.

2. Run "make" to configure, build and test GCC toolchain.

The toolchain binaries are placed under `build-x86_64-linux/usr`.
