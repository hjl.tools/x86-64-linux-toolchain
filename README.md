# x86-64-linux-toolchain

Scripts to build Linux/x86-64 toolchain from source.

- toolchain-build: Supporting scripts for toolchain build.
- gcc-toolchain: Directory to build Linux/x86-64 toolchain with GCC and binutils from source.
